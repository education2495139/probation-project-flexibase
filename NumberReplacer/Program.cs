﻿using NumberReplacer.Core;
using NumberReplacer.Data;

namespace NumberReplacer
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Exercise 1: ");
            IDataSource dataSource1 = new DataSourceString("1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15");
            INumberReplacer numberReplacer1 = new NumberReplacer_fizz_buzz(dataSource1);
            var result1 = String.Join(", ", numberReplacer1.GetResultCollection());
            Console.WriteLine("In: " + result1);
            Console.WriteLine("\n");

            Console.WriteLine("Exercise 2: ");
            IDataSource dataSource2 = new DataSourceString("1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 60, 105, 420");
            NumberReplacerAbstract numberReplacer2Previous = new NumberReplacer_fizz_buzz(dataSource2);
            INumberReplacer numberReplacer2 = new NumberReplacer_muzz_guzz(dataSource2, numberReplacer2Previous);
            var result2 = String.Join(", ", numberReplacer2.GetResultCollection());
            Console.WriteLine("In: " + result2);
            Console.WriteLine("\n");

            Console.WriteLine("Exercise 3: ");
            IDataSource dataSource3 = new DataSourceString("1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 60, 105, 420");
            NumberReplacerAbstract numberReplacer3Previous = new NumberReplacer_good_boy(dataSource3);
            INumberReplacer numberReplacer3 = new NumberReplacer_muzz_guzz(dataSource3, numberReplacer3Previous);
            var result3 = String.Join(", ", numberReplacer3.GetResultCollection());
            Console.WriteLine("In: " + result3);
            Console.WriteLine("\n");
        }
    }
}

