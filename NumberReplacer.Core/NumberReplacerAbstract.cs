﻿using NumberReplacer.Data;

namespace NumberReplacer.Core
{
    public abstract class NumberReplacerAbstract : INumberReplacer
    {
        private protected IDataSource _dataSource;
        private protected IEnumerable<string> _replacedData;

        public NumberReplacerAbstract(IDataSource dataSource)
        {
            _dataSource = dataSource;
            _replacedData = ModifyData();
        }

        public IEnumerable<string> GetResultCollection()
        {
            return _replacedData
                .Select((item, index) => item == string.Empty ? _dataSource.GetData()[index].ToString() : item);
        }

        private IEnumerable<string> ModifyData()
        {
            return _dataSource.GetData()
                .Select(item => GetReplacedWord(item));
        }

        private protected bool IsMultipleOf(int number, int divisor)
        {
            return number % divisor == 0;
        }

        private protected void Concat(ref string str, string add)
        {
            if (str == string.Empty)
            {
                str = add;
            }
            else
            {
                str += "-" + add;
            }
        }

        internal abstract string GetReplacedWord(int number);
    }
}
