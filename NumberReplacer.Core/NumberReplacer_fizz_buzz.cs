﻿using NumberReplacer.Data;

namespace NumberReplacer.Core
{
    public class NumberReplacer_fizz_buzz : NumberReplacerAbstract
    {
        public NumberReplacer_fizz_buzz(IDataSource dataSource) : base(dataSource)
        {
        }

        internal override string GetReplacedWord(int number)
        {
            var str = string.Empty;

            if (IsMultipleOf(number, 3))
            {
                Concat(ref str, "fizz");
            }

            if (IsMultipleOf(number, 5))
            {
                Concat(ref str, "buzz");
            }

            return str;
        }
    }
}
