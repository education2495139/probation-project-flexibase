﻿using NumberReplacer.Data;

namespace NumberReplacer.Core
{
    public class NumberReplacer_good_boy : NumberReplacerAbstract
    {
        public NumberReplacer_good_boy(IDataSource dataSource) : base(dataSource)
        {
        }

        internal override string GetReplacedWord(int number)
        {
            var str = string.Empty;

            if (IsMultipleOf(number, 3) && IsMultipleOf(number, 5))
            {
                Concat(ref str, "good-boy");
                return str;
            }

            if (IsMultipleOf(number, 3))
            {
                Concat(ref str, "dog");
            }

            if (IsMultipleOf(number, 5))
            {
                Concat(ref str, "cat");
            }

            return str;
        }
    }
}
