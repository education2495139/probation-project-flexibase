﻿namespace NumberReplacer.Core
{
    public interface INumberReplacer
    {
        IEnumerable<string> GetResultCollection();
    }
}
