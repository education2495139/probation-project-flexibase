﻿using NumberReplacer.Data;

namespace NumberReplacer.Core
{
    public class NumberReplacer_muzz_guzz : NumberReplacerAbstract
    {
        private NumberReplacerAbstract _numberReplacerPrevious;

        public NumberReplacer_muzz_guzz(IDataSource dataSource, NumberReplacerAbstract numberReplacerParent) 
            : base(dataSource)
        {
            _numberReplacerPrevious = numberReplacerParent;
        }

        internal override string GetReplacedWord(int number)
        {
            var str = _numberReplacerPrevious.GetReplacedWord(number);

            if (IsMultipleOf(number, 4))
            {
                Concat(ref str, "muzz");
            }

            if (IsMultipleOf(number, 7))
            {
                Concat(ref str, "guzz");
            }

            return str;
        }
    }
}
