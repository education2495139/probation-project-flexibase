namespace NumberReplacer.Core.Test
{
    public class NumberReplacer_fizz_buzz_Test
    {
        private int _sequenceSize = 1000;

        [Fact]
        public void GetResultCollection_Collection_NotNull()
        {
            var dataSource = new DataSourceMock(_sequenceSize);
            var result = new NumberReplacer_fizz_buzz(dataSource).GetResultCollection();

            Assert.NotNull(result);
        }

        [Fact]
        public void GetResultCollection_Collection_NotEmpty()
        {
            var dataSource = new DataSourceMock(_sequenceSize);
            var result = new NumberReplacer_fizz_buzz(dataSource).GetResultCollection();

            Assert.NotEmpty(result);
        }

        [Fact]
        public void GetResultCollection_ContainsOnlySpecialChars_IsTrueForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize);
            var result = new NumberReplacer_fizz_buzz(dataSource).GetResultCollection();
            var specialChars = new string("0123456789fizzbuzz-");

            Assert.All<string>(result, item => Assert.All<char>(item, ch => Assert.True(specialChars.Contains(ch))));
        }

        [Fact]
        public void GetResultCollection_WithMultiplesOf3_ReturnFizzForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize, item => item % 3 == 0 && item % 5 != 0);
            var result = new NumberReplacer_fizz_buzz(dataSource).GetResultCollection();

            Assert.All<string>(result, item => Assert.Contains("fizz", item));
        }

        [Fact]
        public void GetResultCollection_WithoutMultiplesOf3_NotReturnFizzForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize, item => item % 3 != 0);
            var result = new NumberReplacer_fizz_buzz(dataSource).GetResultCollection();

            Assert.All<string>(result, item => Assert.DoesNotContain("fizz", item));
        }

        [Fact]
        public void GetResultCollection_WithMultiplesOf5_ReturnBuzzForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize, item => item % 5 == 0 && item % 3 != 0);
            var result = new NumberReplacer_fizz_buzz(dataSource).GetResultCollection();

            Assert.All<string>(result, item => Assert.Contains("buzz", item));
        }

        [Fact]
        public void GetResultCollection_WithoutMultiplesOf5_NotReturnBuzzForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize, item => item % 5 != 0);
            var result = new NumberReplacer_fizz_buzz(dataSource).GetResultCollection();

            Assert.All<string>(result, item => Assert.DoesNotContain("buzz", item));
        }

        [Fact]
        public void GetResultCollection_WithMultiplesOf3And5_ReturnFizzBuzzForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize, item => item % 3 == 0 && item % 5 == 0);
            var result = new NumberReplacer_fizz_buzz(dataSource).GetResultCollection();

            Assert.All<string>(result, item => Assert.Contains("fizz-buzz", item));
        }

        [Fact]
        public void GetResultCollection_WithoutMultiplesOf3And5_ReturnNumberForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize, item => item % 3 != 0 && item % 5 != 0);
            var result = new NumberReplacer_fizz_buzz(dataSource).GetResultCollection();
            var variants = new List<string> { "fizz", "buzz", "fizz-buzz" };

            Assert.All<string>(result, item => Assert.All<string>(variants, v => Assert.DoesNotContain(v, item)));
        }
    }
}
