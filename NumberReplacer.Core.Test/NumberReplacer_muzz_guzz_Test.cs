using NumberReplacer.Data;

namespace NumberReplacer.Core.Test
{
    public class NumberReplacer_muzz_guzz_Test
    {
        private int _sequenceSize = 1000;

        [Fact]
        public void GetResultCollection_Collection_NotNull()
        {
            var dataSource = new DataSourceMock(_sequenceSize);
            NumberReplacerAbstract previousNumberReplacer = new NumberReplacer_fizz_buzz(dataSource);
            var result = new NumberReplacer_muzz_guzz(dataSource, previousNumberReplacer).GetResultCollection();

            Assert.NotNull(result);
        }

        [Fact]
        public void GetResultCollection_Collection_NotEmpty()
        {
            var dataSource = new DataSourceMock(_sequenceSize);
            NumberReplacerAbstract previousNumberReplacer = new NumberReplacer_fizz_buzz(dataSource);
            var result = new NumberReplacer_muzz_guzz(dataSource, previousNumberReplacer).GetResultCollection();

            Assert.NotEmpty(result);
        }

        [Fact]
        public void GetResultCollection_WithMultiplesOf4_ReturnMuzzForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize, item => item % 4 == 0 && item % 7 != 0);
            NumberReplacerAbstract previousNumberReplacer = new NumberReplacer_fizz_buzz(dataSource);
            var result = new NumberReplacer_muzz_guzz(dataSource, previousNumberReplacer).GetResultCollection();

            Assert.All<string>(result, item => Assert.Contains("muzz", item));
        }

        [Fact]
        public void GetResultCollection_WithoutMultiplesOf4_NotReturnMuzzForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize, item => item % 4 != 0);
            NumberReplacerAbstract previousNumberReplacer = new NumberReplacer_fizz_buzz(dataSource);
            var result = new NumberReplacer_muzz_guzz(dataSource, previousNumberReplacer).GetResultCollection();

            Assert.All<string>(result, item => Assert.DoesNotContain("muzz", item));
        }

        [Fact]
        public void GetResultCollection_WithMultiplesOf7_ReturnGuzzForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize, item => item % 7 == 0 && item % 4 != 0);
            NumberReplacerAbstract previousNumberReplacer = new NumberReplacer_fizz_buzz(dataSource);
            var result = new NumberReplacer_muzz_guzz(dataSource, previousNumberReplacer).GetResultCollection();

            Assert.All<string>(result, item => Assert.Contains("guzz", item));
        }

        [Fact]
        public void GetResultCollection_WithoutMultiplesOf7_NotReturnGuzzForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize, item => item % 7 != 0);
            NumberReplacerAbstract previousNumberReplacer = new NumberReplacer_fizz_buzz(dataSource);
            var result = new NumberReplacer_muzz_guzz(dataSource, previousNumberReplacer).GetResultCollection();

            Assert.All<string>(result, item => Assert.DoesNotContain("guzz", item));
        }

        [Fact]
        public void GetResultCollection_WithMultiplesOf4And7_ReturnMuzzGuzzForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize, item => item % 4 == 0 && item % 7 == 0);
            NumberReplacerAbstract previousNumberReplacer = new NumberReplacer_fizz_buzz(dataSource);
            var result = new NumberReplacer_muzz_guzz(dataSource, previousNumberReplacer).GetResultCollection();

            Assert.All<string>(result, item => Assert.Contains("muzz-guzz", item));
        }

        [Fact]
        public void GetResultCollection_WithoutMultiplesOf4And7_ReturnNumberForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize, item => item % 4 != 0 && item % 7 != 0);
            NumberReplacerAbstract previousNumberReplacer = new NumberReplacer_fizz_buzz(dataSource);
            var result = new NumberReplacer_muzz_guzz(dataSource, previousNumberReplacer).GetResultCollection();
            var variants = new List<string> { "muzz", "guzz", "muzz-guzz" };

            Assert.All<string>(result, item => Assert.All<string>(variants, v => Assert.DoesNotContain(v, item)));
        }
    }
}
