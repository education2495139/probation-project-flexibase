namespace NumberReplacer.Core.Test
{
    public class NumberReplacer_good_boy_Test
    {
        private int _sequenceSize = 1000;

        [Fact]
        public void GetResultCollection_Collection_NotNull()
        {
            var dataSource = new DataSourceMock(_sequenceSize);
            var result = new NumberReplacer_good_boy(dataSource).GetResultCollection();

            Assert.NotNull(result);
        }

        [Fact]
        public void GetResultCollection_Collection_NotEmpty()
        {
            var dataSource = new DataSourceMock(_sequenceSize);
            var result = new NumberReplacer_good_boy(dataSource).GetResultCollection();

            Assert.NotEmpty(result);
        }

        [Fact]
        public void GetResultCollection_ContainsOnlySpecialChars_IsTrueForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize);
            var result = new NumberReplacer_good_boy(dataSource).GetResultCollection();
            var specialChars = new string("0123456789dogcatgoodboy-");

            Assert.All<string>(result, item => Assert.All<char>(item, ch => Assert.True(specialChars.Contains(ch))));
        }

        [Fact]
        public void GetResultCollection_WithMultiplesOf3_ReturnDogForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize, item => item % 3 == 0 && item % 5 != 0);
            var result = new NumberReplacer_good_boy(dataSource).GetResultCollection();

            Assert.All<string>(result, item => Assert.Contains("dog", item));
        }

        [Fact]
        public void GetResultCollection_WithoutMultiplesOf3_NotReturnDogForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize, item => item % 3 != 0);
            var result = new NumberReplacer_good_boy(dataSource).GetResultCollection();

            Assert.All<string>(result, item => Assert.DoesNotContain("dog", item));
        }

        [Fact]
        public void GetResultCollection_WithMultiplesOf5_ReturnCatForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize, item => item % 5 == 0 && item % 3 != 0);
            var result = new NumberReplacer_good_boy(dataSource).GetResultCollection();

            Assert.All<string>(result, item => Assert.Contains("cat", item));
        }

        [Fact]
        public void GetResultCollection_WithoutMultiplesOf5_NotReturnCatForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize, item => item % 5 != 0);
            var result = new NumberReplacer_good_boy(dataSource).GetResultCollection();

            Assert.All<string>(result, item => Assert.DoesNotContain("cat", item));
        }

        [Fact]
        public void GetResultCollection_WithMultiplesOf3And5_ReturnGoodBoyForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize, item => item % 3 == 0 && item % 5 == 0);
            var result = new NumberReplacer_good_boy(dataSource).GetResultCollection();

            Assert.All<string>(result, item => Assert.Contains("good-boy", item));
        }

        [Fact]
        public void GetResultCollection_WithoutMultiplesOf3And5_ReturnNumberForAllElements()
        {
            var dataSource = new DataSourceMock(_sequenceSize, item => item % 3 != 0 && item % 5 != 0);
            var result = new NumberReplacer_good_boy(dataSource).GetResultCollection();
            var variants = new List<string> { "dog", "cat", "good-boy" };

            Assert.All<string>(result, item => Assert.All<string>(variants, v => Assert.DoesNotContain(v, item)));
        }
    }
}
