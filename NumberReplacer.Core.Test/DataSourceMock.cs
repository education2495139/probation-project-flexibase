namespace NumberReplacer.Core.Test
{
    public class DataSourceMock : IDataSource
    {
        private protected readonly IList<int> data;

        public DataSourceMock(int countNumbers, Func<int, bool> condition)
        {
            data = new List<int>();

            FillMockData(countNumbers, condition);
        }

        public DataSourceMock(int countNumbers) : this(countNumbers, item => true )
        {
        }

        public IList<int> GetData()
        {
            return data;
        }

        private void FillMockData(int countNumbers, Func<int, bool> condition)
        {
            for (int k = 1; k <= countNumbers; k++)
            {
                if (condition.Invoke(k))
                {
                    data.Add(k);
                }
            }
        }
    }
}
