﻿namespace NumberReplacer.Data
{
    public interface IDataSource
    {
        IList<int> GetData();
    }
}
