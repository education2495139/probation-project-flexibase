﻿namespace NumberReplacer.Data
{
    public class DataSourceString : IDataSource
    {
        private readonly IEnumerable<int> _data;

        public DataSourceString(string dataSourceString)
        {
            _data = LoadDataFromFile(dataSourceString);
        }

        public IList<int> GetData()
        {
            return _data.ToList();
        }

        private IEnumerable<int> LoadDataFromFile(string dataSourceString)
        {
            return dataSourceString.Split(", ").Select(item => int.Parse(item));
        }
    }
}
