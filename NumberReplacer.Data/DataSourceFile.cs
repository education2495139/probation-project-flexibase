﻿namespace NumberReplacer.Data
{
    public class DataSourceFile : IDataSource
    {
        private readonly IEnumerable<int> _data;

        public DataSourceFile(string fileName)
        {
            _data = LoadDataFromFile(fileName);
        }

        public IList<int> GetData()
        {
            return _data.ToList();
        }

        private IEnumerable<int> LoadDataFromFile(string fileName)
        {
            string[] path = { Environment.CurrentDirectory, "DataFiles", fileName };

            using (StreamReader file = File.OpenText(Path.Combine(path)))
            {
                return file.ReadToEnd().Split(", ").Select(item => int.Parse(item));
            }
        }
    }
}
